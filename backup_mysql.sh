#!/bin/bash
# ------------------------------------------------------------------- #
#   BACKUP de toutes les bases MySQL d'un utilisateur
#   -------------------------------------------------
#
#   Auteur : DA COSTA LEITAO Nicolas
#   Site : http://www.ndacostaleitao.info/
#   Date : 11/08/2017
#
#   Principe :
#   ---------
#   Sauvegarde les bases de donnÃ©es d'un utilisateur chez FREE.FR dans 
#   un fichier .sql puis compresse les fichiers dans un .gz
#   Permet de purger les anciennes sauvegardes
#   Permet aussi de recevoir l'archive par mail.
#   Renseigne ses actions dans un fichier log et poste par mail
#   un rapport de ses actions (activable ou non)
#
# --------------------------------------------------------------------- #
 
# --------------------------------------------------------------------- #
#   PARAMETRES DU SCRIPT
# --------------------------------------------------------------------- #
#
# Login SQL
LOGIN=
#Password SQL
PASSWORD=
# Nom du Fichier
NAME=
# Date du jour
DATE="$(date +"%d-%m-%Y")"
# Répertoire de sauvegarde
BACKUPDIR=
# LOGS
BACKUPDIRLOG=/var/log/save_sql/backup_$NAME_$DATE.log
# Cible + Format de sauvegarde
ARCHIVES=$BACKUPDIR/$NAME.$DATE.sql.gz
# Adresse mail de destination
EMAILID=root
# --------------------------------------------------------------------- #
#   FIN DES PARAMETRES, ne rien changer ensuite                         #
# --------------------------------------------------------------------- #
#**************************Fonction**************************************
gestion_retour() {
	if [ $1 -eq 0 ]; then
		echo "\033[1;32m ... OK \033[0m"
	else
		echo "... ERREUR CODE $? " >> $BACKUPDIRLOG
		mail -s "Echec BACKUP_MYSQL_"${NAME}"_du_"${DATE}"" "$EMAILID" < $BACKUPDIRLOG
		exit 1
	fi
}
prestart() {
	if [ -d "$BACKUPDIR" ]; then            
		echo "\033[1;32m Le répertoires BACKUPDIR existe déjà ... \033[0m"
		echo "\033[1;32m Préparation du téléchargement \033[0m" && cd $BACKUPDIR                         
	else
	echo "Les répertoires BACKUPDIR n'existe pas ! arrêt de la sauvegarde..." >> $BACKUPDIRLOG && gestion_retour +1
	fi 
}
#**************************debut du script********************************
echo "\033[1;35m Prévérification...en cours... \033[0m"
prestart
#ecriture du log de debut
echo "[`date +"%d/%m/%y"-"%H:%M"`] Debut de la sauvegarde" >>$BACKUPDIRLOG
sleep 2
echo "\033[1;35m Telechargement de la sauvegarde...en cours... \033[0m"
# telechargement de la sauvegarde
wget http://sql.free.fr/backup.php --post-data="login=$LOGIN&password=$PASSWORD&check=1&all=1" -O $ARCHIVES && gestion_retour $?
sleep 2
echo "\033[1;35m Fin de la sauvegarde ! \033[0m"
echo "[`date +"%d/%m/%y"-"%H:%M"`] Fin de la sauvegarde" >>$BACKUPDIRLOG
sleep 2
#copie backup a conserver dans un autre répertoire
cp -ur --verbose $ARCHIVES $BACKUPDIROLD && gestion_retour $?
echo "[`date +"%d/%m/%y"-"%H:%M"`] Debut du nettoyage" >> $BACKUPDIRLOG
#nombre de jour de backup a conserver
echo "\033[1;35m Nettoyage en cours... \033[0m"
find . -name '*.sql.gz' -mtime +35 | xargs rm -f && gestion_retour $?
echo "[`date +"%d/%m/%y"-"%H:%M"`] Fin du nettoyage" >> $BACKUPDIRLOG
echo "\033[1;35m Fin du traitement ! \033[0m"
mail -s "BACKUP_MYSQL_"${NAME}"_du_"${DATE}"" "$EMAILID" < $BACKUPDIRLOG